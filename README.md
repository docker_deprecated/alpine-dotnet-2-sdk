# alpine-dotnet-2-sdk

#### [alpine-x64-dotnet-2-sdk](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-dotnet-2-sdk/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinex64/alpine-x64-dotnet-2-sdk/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinex64/alpine-x64-dotnet-2-sdk/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinex64/alpine-x64-dotnet-2-sdk/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-dotnet-2-sdk)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-dotnet-2-sdk)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64
* Appplication : [.NET Core](https://github.com/dotnet/core/)
    - .NET Core is a free and open-source managed computer software framework for the Windows, Linux, and macOS operating systems.



----------------------------------------
#### Run

```sh
docker run -i -t --rm \
           forumi0721alpine[ARCH]/alpine-[ARCH]-dotnet-2-sdk:latest
```



----------------------------------------
#### Usage

* SDK



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

